import { itemDetails } from "src/mocks/itemDetails";

export default function handler(req, res) {
  const splitUrl = req.url.split("/");
  const ids = splitUrl[splitUrl.length - 1].split(",");

  const someStuff = [];

  for (let i = 0; i < ids.length; i++) {
    const id = ids[i];
    const itemData = itemDetails.find((item) => {
      return item.id === id;
    });
    if (itemData) {
      someStuff.push(itemData);
    }
  }

  setTimeout(() => {
    res.status(200).json({ idDebug: ids, items: someStuff });
  }, 1000);
}
