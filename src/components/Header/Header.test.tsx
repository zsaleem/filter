import { render, screen } from "@testing-library/react";
// import userEvent from "@testing-library/user-event";
import React from "react";
import { withTheme } from "src/hocs/withTheme";
import Header from "./Header";

const ThemedHeader = withTheme(Header);

describe("Header", () => {
  test("renders text", () => {
    const headerTestId = "header";
    render(<ThemedHeader />);
    const header = screen.getByTestId(headerTestId);
    expect(header).toBeInTheDocument();
  });
});
