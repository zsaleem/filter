export interface HeaderType {
  accent?: string;
  bgColor?: string;
}

export interface BackgroundProps {
  accent?: string;
  bgColor?: string;
}
