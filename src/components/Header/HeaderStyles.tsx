import styled from "styled-components";
import { BackgroundProps } from "./types";

export const Background = styled.section<BackgroundProps>`
  border-top: 11px solid ${({ accent }) => accent};
  background-color: ${({ bgColor }) => bgColor};
  height: 100px;
  padding: 16px 22px 16px 41px;
`;
