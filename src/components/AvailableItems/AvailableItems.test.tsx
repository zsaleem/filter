import { render, screen } from "@testing-library/react";
// import userEvent from "@testing-library/user-event";
import React from "react";
import { withTheme } from "src/hocs/withTheme";
import AvailableItems from "./AvailableItems";

const ThemedAvailableItems = withTheme(AvailableItems);

describe("AvailableItems", () => {
  test("renders one of the titles", () => {
    const text = "Phone";
    render(<ThemedAvailableItems setSelectedItems={jest.fn()} selectedItems={[]} setActiveId={jest.fn()} activeId={[]} />);
    const phoneLabel = screen.getByText(text);
    expect(phoneLabel).toBeInTheDocument();
  });
});
