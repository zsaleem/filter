import { ConnotationColorEnum, IconTile, Toast, ToastHandler } from "cerebellum";
import React, { FC, useCallback, useRef } from "react";

import { availableItems } from "src/mocks/availableItems";
import { AvailableItemsBase, Drawer, ItemBox, Text, WhiteBackground } from "./AvailableItemsStyles";
import { ActiveIdParams, AvailableItemsType } from "./types";

export const AvailableItems: FC<AvailableItemsType> = ({ setSelectedItems, selectedItems, setActiveId, activeId }) => {
  const isToastActive = useRef(false);

  const showToast = ({ colorFamily, text, title }) => {
    if (isToastActive.current === false) {
      ToastHandler.show({
        colorFamily,
        text,
        title,
      });
      isToastActive.current = true;
      setTimeout(() => {
        isToastActive.current = false;
      }, 5000);
    }
  };

  const setFilteredActiveId = useCallback(
    ({ label }: ActiveIdParams) => {
      setActiveId((prev) => filterSelections({ item: label, list: activeId, prev }));
    },
    [activeId]
  );

  const setFilteredSelectedItems = useCallback(
    ({ id }: { id: string }) => {
      setSelectedItems((prev) => filterSelections({ item: id, list: selectedItems, prev, listType: "selectedItems" }));
    },
    [selectedItems]
  );

  const filterSelections = ({ item, list = [], prev, listType = null }) => {
    if (list.includes(item)) {
      let index = list.indexOf(item);
      return [...list.slice(0, index), ...list.slice(index + 1)];
    }
    if (list.length > 2) {
      if (listType === "selectedItems") {
        showToast({
          text: "To many selections. You may select a maximum of 3 items.",
          colorFamily: ConnotationColorEnum.Neutral,
        });
      }
      return [...list];
    }

    return [...prev, item];
  };

  return (
    <AvailableItemsBase>
      <Drawer>
        <Text>Things to Bring:</Text>
        {availableItems.map(({ color, icon, label, id }) => {
          return (
            <ItemBox key={label}>
              <IconTile
                active={activeId?.includes(label)}
                onButtonClick={() => {
                  setFilteredActiveId({ label });
                  setFilteredSelectedItems({ id });
                }}
                label={label}
                Icon={icon}
                colorFamily={color}
              />
            </ItemBox>
          );
        })}
      </Drawer>
      <WhiteBackground />
      <Toast ref={(ref) => ToastHandler.setRef(ref)} />
    </AvailableItemsBase>
  );
};

AvailableItems.defaultProps = {};

export default AvailableItems;
