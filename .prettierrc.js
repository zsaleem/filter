module.exports = {
  arrowParens: "always",
  printWidth: 140,
  semi: true,
  singleQuote: false,
  tabWidth: 2,
  trailingComma: "es5",
};
