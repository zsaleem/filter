import { render, screen } from "@testing-library/react";
// import userEvent from "@testing-library/user-event";
import React from "react";
import { withTheme } from "src/hocs/withTheme";
import { itemDetails } from "src/mocks/itemDetails";
import ItemsToBring from "./ItemsToBring";
import { Text } from "./ItemsToBringStyles";

const ThemedItemsToBring = withTheme(ItemsToBring);

describe("ItemsToBring", () => {
  test("renders text", () => {
    const firstItem = itemDetails[0];
    const { container } = render(<ThemedItemsToBring selectedItems={[`firstItem.id`]} deSelectItems={jest.fn()} deActiveId={jest.fn()} />);
    const text = container.querySelector("p");
    expect(text).toBeInTheDocument();
  });
});
