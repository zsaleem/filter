export interface ItemsToBringType {
  selectedItems: Array<string>;
  deSelectItems: () => void;
  activeId: ItemDetailsType[];
  deActiveId: () => void;
}

export interface ItemDetailsType {
  description?: string;
  id: string;
  label: string;
  location?: string;
}
export interface BackgroundProps {
  bgColor?: string;
}

export interface TileSubLabelProps {
  contentColor?: string;
  lblColor?: string;
}
