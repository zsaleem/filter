import { colors } from "cerebellum";
import React, { FC } from "react";

import { Background } from "./HeaderStyles";
import { HeaderType } from "./types";

export const Header: FC<HeaderType> = ({ accent, bgColor }) => {
  return <Background data-testid="header" accent={accent} bgColor={bgColor} />;
};

Header.defaultProps = {
  accent: "transparent",
  bgColor: colors.WHITE,
};

export default Header;
