import { Close, colors } from "cerebellum";
import styled from "styled-components";
import { BackgroundProps, TileSubLabelProps } from "./types";

export const Container = styled.section`
	max-width: 1440px;
`;

export const Background = styled.section<BackgroundProps>`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 23px;
  background: ${({ bgColor }) => bgColor};
  padding: 16px 22px 16px 41px;

  @media all and (max-width: 1210px) {
    grid-template-columns: 1fr 1fr;
  }
  @media all and (max-width: 660px) {
    grid-template-columns: 1fr;
  }
`;

export const Tile = styled.section`
  position: relative;
  display: grid;
  grid-template-columns: 1fr 4fr;
  padding: 25px 21px 39px 23px;
  background-color: #ffffff;
  border-radius: 18px;
  box-shadow: 0 6px 40px 0.8px rgba(93, 122, 213, 0.07);
`;

export const TileColumn = styled.section``;

export const TileLabel = styled.h3`
  margin: 20px 0;
  font-size: 22px;
  line-height: 30px;
  color: #00010D;
  font-weight: bold;
  letter-spacing: 0;
`;

export const TileSubLabel = styled.p<TileSubLabelProps>`
  margin-bottom: 16px;
  color: ${({ contentColor }) => contentColor};
  font-size: 16px;
  font-weight: 600;
  letter-spacing: 0;
  line-height: 24px;

  span {
    margin-right: 12px;
    color: ${({ lblColor }) => lblColor};
  }
`;

export const Text = styled.h2`
  padding-top: 62px;
  padding-bottom: 23px;
  padding-left: 40px;
  color: #00010d;
  font-size: 16px;
  font-weight: bold;
  letter-spacing: 0;
  line-height: 22px;
  background-color: ${colors.COOL_GREY_4};
`;

export const CloseBtn = styled.button`
  position: absolute;
  right: 18.16px;
  top: 10.66px;
  height: 12.68px;
  width: 12.69px;
  color: #A5ACBE;
  font-size: 30px;
  background: none;
  transform: rotate(45deg);
`;