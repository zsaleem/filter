export const itemDetails = [
  {
    description: "An iPhone in an orange case.",
    id: "id_1",
    label: "Phone",
    location:
      "Hopefully in my top left dresser drawer, but it's probably just sitting on the kitchen counter. I really need to take better care of my things.",
  },
  {
    description: "Silver device with a black strap in a small, grey, drawstring pouch.",
    id: "id_2",
    label: "Heart Monitor",
  },
  {
    description:
      "A contract for the lease extension, with Parker Brothers, LLC. It should have red sticky notes attached and be about 7 pages long.",
    id: "id_3",
    label: "Paperwork",
    location: "In the 'Urgent' paperwork tray on my desk.",
  },
  {
    description: "My company ID badge, on a lanyard",
    id: "id_4",
    label: "ID Badge",
    location: "Hanging on the arm of my chair. Or possibly in the desk drawer. Check under my chair, too.",
  },
  {
    description: "Contact lense case and eye drops",
    id: "id_5",
    label: "Eye Care",
    location: "In the kitchen cabinet, closest to the front door. Or possibly in the bathroom, by the sink.",
  },
  {
    id: "id_6",
    label: "Health Card",
    location: "In the dish of random stuff by the garage door, where the spare key is.",
  },
];
