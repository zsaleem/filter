import { ButtonColorFamilyEnum, Clipboard, Document, Employee, Eye, Heart, Mobile } from "cerebellum";

// 'label' is used for a key and must be unique.
export const availableItems = [
  {
    color: ButtonColorFamilyEnum.Blue,
    id: "id_1",
    icon: Mobile,
    label: "Phone",
  },
  {
    color: ButtonColorFamilyEnum.Red,
    id: "id_2",
    icon: Heart,
    label: "Heart Monitor",
  },
  {
    color: ButtonColorFamilyEnum.CoolGrey,
    id: "id_3",
    icon: Document,
    label: "Paperwork",
  },
  {
    color: ButtonColorFamilyEnum.Peach,
    id: "id_4",
    icon: Employee,
    label: "ID Badge",
  },
  {
    color: ButtonColorFamilyEnum.Purple,
    id: "id_5",
    icon: Eye,
    label: "Eye Care",
  },
  {
    color: ButtonColorFamilyEnum.Aqua,
    id: "id_6",
    icon: Clipboard,
    label: "Health Card",
  },
];
