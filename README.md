## Setup

After you clone the repo and run `yarn install`, the app should run with `yarn dev`. You can view it at `http://localhost:3000/`

## Running tests

`yarn test`, `yarn test --watch`, and `yarn test --watch -i ComponentName` are your friends.
