import { Volunteer, Mobile, Document, Employee, Eye, Orders, colors } from "cerebellum";
import React, { FC } from "react";
import useSwr from "swr";

import { Background, CloseBtn, Text, Tile, TileColumn, TileLabel, TileSubLabel, Container } from "./ItemsToBringStyles";
import { ItemDetailsType, ItemsToBringType } from "./types";

const a = (url: string) => fetch(url).then((res) => res.json());

const iconsMap = {
  "Heart Monitor": <Volunteer round fill={colors.RED_90} boxColor={colors.RED_5} />,
  "Phone": <Mobile round fill={colors.BLUE_100} boxColor={colors.BLUE_5} />,
  "Paperwork": <Document round fill={colors.COOL_GREY_100} boxColor={colors.COOL_GREY_15} />,
  "ID Badge": <Employee round fill={colors.PEACH_100} boxColor={colors.PEACH_5} />,
  "Eye Care": <Eye round fill={colors.PURPLE_100} boxColor={colors.PURPLE_5} />,
  "Health Card": <Orders round fill={colors.AQUA_100} boxColor={colors.AQUA_5} />,
};

export const ItemsToBring: FC<ItemsToBringType> = ({ deSelectItems, selectedItems, deActiveId }) => {
  const { data, error } = useSwr(`/api/stuffInDesk/${selectedItems}`, a);

  if (error) return <p>Failed to load desk contents</p>;

  const items = data?.items;

  return (
    <Container>
      <Text>Location Details:</Text>
      <Background bgColor={colors.COOL_GREY_4}>
        {items?.map(({ label, description, location, id }: ItemDetailsType, index: number) => {
          return (
            <Tile key={`${label}-${index}`}>
              {iconsMap[label]}
              <TileColumn>
                <TileLabel>{label}</TileLabel>
                <TileSubLabel lblColor={colors.COOL_GREY_60} contentColor={colors.COOL_GREY_100}>
                  <span>Location: </span>
                  {location}
                </TileSubLabel>
                <TileSubLabel lblColor={colors.COOL_GREY_60} contentColor={colors.COOL_GREY_100}>
                  <span>Description: </span>
                  {description}
                </TileSubLabel>
                <CloseBtn
                  onClick={() => {
                    deSelectItems({ id });
                    deActiveId({ label });
                  }}
                >
                  +
                </CloseBtn>
              </TileColumn>
            </Tile>
          );
        })}
      </Background>
    </Container>
  );
};

ItemsToBring.defaultProps = {};

export default ItemsToBring;
