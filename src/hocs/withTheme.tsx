import { cerebellumTheme } from "cerebellum";
import React, { FC } from "react";
import { ThemeProvider } from "styled-components";

export function withTheme<P>(WrappedComponent: React.ComponentType<P>): FC<P> {
  const ComponentWithTheme = (props: P) => {
    return (
      <ThemeProvider theme={cerebellumTheme}>
        <WrappedComponent {...props} />
      </ThemeProvider>
    );
  };
  return ComponentWithTheme;
}
export default withTheme;
