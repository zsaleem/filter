module.exports = {
  testEnvironment: "jsdom",
  moduleNameMapper: {
    "\\.(css|less|scss)$": "identity-obj-proxy",
  },
  setupFilesAfterEnv: ["<rootDir>/jest-setup.ts"],
  modulePaths: ["<rootDir>"],
  roots: ["<rootDir>"],
  moduleDirectories: ["node_modules", "src"],
};
