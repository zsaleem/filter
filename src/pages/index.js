import { colors } from "cerebellum";
import { useCallback, useState } from "react";
import { AvailableItems, Header, ItemsToBring } from "src/components";

export default function Home() {
  const [selectedItems, setSelectedItems] = useState([]);
  const [activeId, setActiveId] = useState([]);

  const deSelectItems = useCallback(
    ({ id }) => {
      setSelectedItems(() => filterList({ itemTobeRemoved: id, list: selectedItems }));
    },
    [selectedItems]
  );

  const deActiveId = useCallback(
    ({ label }) => {
      setActiveId(() => filterList({ itemTobeRemoved: label, list: activeId }));
    },
    [activeId]
  );

  const filterList = ({ itemTobeRemoved, list }) => {
    const index = list.indexOf(itemTobeRemoved);
    if (list.includes(itemTobeRemoved)) {
      return [...list.slice(0, index), ...list.slice(index + 1)];
    }
  };

  return (
    <main>
      <Header text="Things to bring" accent={colors.BLUE_50} />
      <AvailableItems setSelectedItems={setSelectedItems} selectedItems={selectedItems} setActiveId={setActiveId} activeId={activeId} />
      <ItemsToBring deSelectItems={deSelectItems} selectedItems={selectedItems} activeId={activeId} deActiveId={deActiveId} />
    </main>
  );
}
