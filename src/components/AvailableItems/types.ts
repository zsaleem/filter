export interface AvailableItemsType {
  setSelectedItems: (id: string) => void;
  setActiveId: (id: string) => void;
  activeId: ActiveIds[];
  selectedItems: Items[];
}

interface ActiveIds {
  label: string;
  description: string;
  id: string;
}

interface Items {
  id: string;
}

export interface ActiveIdParams {
  label: string;
}

export interface ToastParams {
  colorFamily: string;
  text: string;
  title: string;
}

export interface FilterSelectionsParam {
  item: string;
  list: Array<string>;
  prev: Array<string>;
  listType: string | null;
}
