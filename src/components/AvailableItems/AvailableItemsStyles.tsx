import { colors } from "cerebellum";
import styled from "styled-components";

export const AvailableItemsBase = styled.section`
  background-color: ${colors.COOL_GREY_4};
  position: relative;
`;
export const Drawer = styled.div`
  margin: 0 auto;
  max-width: 1202px;
  padding: 16px 22px;
  position: relative;
  text-align: center;
  width: 100%;
  z-index: 1;
  @media all and (max-width: 1210px) {
    width: 625px;
  }
  @media all and (max-width: 660px) {
    max-width: 100%;
    width: 432px;
  }
`;
export const Text = styled.h1`
  color: ${colors.COOL_GREY_100};
  font-size: 40px;
  font-weight: 700;
  line-height: 54px;
  margin-left: 20px;
  text-align: left;
`;

export const ItemBox = styled.div`
  display: inline-block;
  margin: 39px 20px 20px;
`;
export const WhiteBackground = styled.div`
  background-color: ${colors.WHITE};
  height: 231px;
  left: 0;
  position: absolute;
  right: 0;
  top: 0;
`;
